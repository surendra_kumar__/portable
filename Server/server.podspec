Pod::Spec.new do |s|
s.name         = "Server"
s.version      = '14.0'
s.summary      = 'Auth for iOS'
s.homepage     = 'https://bitbucket.org/surendra_kumar__/portable/src/master'
s.authors      = 'Photon'
s.source       = { :git =>'https://surendra7700@bitbucket.org/surendra_kumar__/podspecs.git', :submodules => true, :branch => 'master'}
s.source_files = 'Auth/**/*.{h,m,mm,swift}'
s.requires_arc = true
s.framework    = 'Foundation', 'UIKit'

pch_AF = <<-EOS
#ifndef TARGET_OS_IOS
#define TARGET_OS_IOS TARGET_OS_IPHONE
#endif
#ifndef TARGET_OS_WATCH
#define TARGET_OS_WATCH 0
#endif
#ifndef TARGET_OS_TV
#define TARGET_OS_TV 0
#endif
EOS

s.swift_version = '5.0'
s.ios.deployment_target = '11.0'

s.dependency 'Alamofire', '5.5'


end
