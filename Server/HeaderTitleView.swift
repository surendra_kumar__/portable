//
//  HeaderTitleView.swift
//  Semanoor
//
//  Created by Nooor_Testflight on 26/05/21.
//  Copyright © 2021 Nooor. All rights reserved.
//

import UIKit
//test
class HeaderTitleView: UIView {
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var searchBarBtn: UIButton!
    
    var myClosure: ((String) -> (Void))?


    let nibName = "HeaderTitleView"
      
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
      func commonInit() {
        Bundle.main.loadNibNamed("HeaderTitleView", owner: self, options: nil)
        customView.frame = self.bounds
        addSubview(customView)
        self.localization()
      }
    
    
    func localization(){
    }
    
    @IBAction func goHome(_ sender: Any) {
        let helloString = "hello"
        myClosure!(helloString)
    }
    
    @IBAction func userProfileAction(_ sender: Any) {
        
    }
}
