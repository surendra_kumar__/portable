//
//  ContactDetailsModel.swift
//  VishApp
//
//  Created by susheel chennaboina on 21/02/21.
//

import Foundation


//test surendra

// MARK: - Response
struct ResponseData: Codable {
    let _id, userid, name, mob_no: String?
    let email, occupation, designation, company: String?
    let website, worktimein, worktimeout, address, businesslogo, latitude, longitude: String?
}


// MARK: - OTPGenerateRequest
struct ContactDetailsRequest: Codable {
    let userid: String
    let name: String
    let mob_no: String
    let email: String
    let occupation: String
    let designation: String
    let company: String
    let website: String
    let worktimein: String
    let worktimeout: String
    let address: String
    let businesslogo: String
    let latitude: String
    let longitude: String
}

struct ContactDetailsError: Codable {
    let code : String
    let status: String
    let response: String
}

