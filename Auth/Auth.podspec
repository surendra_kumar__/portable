Pod::Spec.new do |s|
s.name         = 'Auth'
s.version      = '14.0'
s.summary      = 'Auth for iOS'
s.homepage     = 'https://surendra7700@bitbucket.org/surendra_kumar__/portable.git'
s.authors      = 'Saudi'
s.source       = { :git =>'https://surendra7700@bitbucket.org/surendra_kumar__/portable.git', :submodules => true, :branch => 'master'}
s.source_files = 'Auth/**/*.{h,m,mm,swift}'
s.requires_arc = true
s.framework    = 'Foundation', 'UIKit'

pch_AF = <<-EOS
#ifndef TARGET_OS_IOS
#define TARGET_OS_IOS TARGET_OS_IPHONE
#endif
#ifndef TARGET_OS_WATCH
#define TARGET_OS_WATCH 0
#endif
#ifndef TARGET_OS_TV
#define TARGET_OS_TV 0
#endif
EOS

s.swift_version = '5.0'
s.ios.deployment_target = '11.0'

s.dependency 'AppAuth', '1.4.0'


# s.resource_bundles = {
#    '${POD_NAME}' => ['${POD_NAME}/Assets/*.png']
#  }


 s.resource_bundles = {'Auth' => [
 'Auth/**/*.{png,storyboard,xib,bundle}'
 ]
}

end
